#! /usr/bin/python3
# Python3 code to print IP addresses and host name of local network
# to screen

import nmap
import tkinter


class Application(tkinter.Tk):
	"""A GUI aplication showing nmap output"""

	def __init__(self, parent, hosts):
		"""Initialise the frame"""
		tkinter.Tk.__init__(self, parent)
		self.parent = parent
		self.hosts = hosts
		self.initilize()
		self.var = StringVar()

	def initilize(self):
		self.grid()
		self.create_widgets()

	def create_widgets(self):
		"""Creates widgets for nmap application """
		self.list = tkinter.Message( textvariable=self.var, releif=RAISED)
		tkinter.Label(self, text = "Running machines on this network").grid(row= 0, column = 0 )
		#tkinter.Label(self, text = str(devices.hosts[1]))
		self.list.set(self.hosts.get_hosts)



class get_hostnames(object):
	'''Scans the network specified (192.168.54.1 by default)'''
	def __init__(self, network='192.168.54.0'):
		'''network takes the network to be scanned, for example 192.168.1.0'''
		self.network = network
		self.nm = nmap.PortScanner()
		self.hosts = []

	def scan(self):
		'''
		Carries out a nmap scan of the network
		Appends the scan results to a list in the format
		IP address : Host name
		'''
		self.nm.scan(hosts=self.network + '/24',arguments='-sP')
		for host in self.nm.all_hosts():
			self.hosts.append('{0}	{1}'.format(host, self.nm[host].hostname()))
			self.hosts.sort()

	def print_hosts(self):
		'''Prints the list of devices, hostname and IP address to console'''
		print("Lists of hosts on this network")
		for host in range(len(self.hosts)):
			print(self.hosts[host])

	def get_hosts(self):
		self.scan()
		return self.hosts

if __name__ == "__main__":

	devices = get_hostnames()
	devices.scan()
	devices.print_hosts()


	#create the window
	app = Application(None, hosts=devices)

	#modify root window
	app.title("Ipswich Makerspace - Devices")
	app.geometry("500x300")

	#Kick off the event loop
	app.mainloop()
