#! /usr/bin/python3
# Python3 code to print IP addresses and host name of local network
# to screen

import nmap
from tkinter import *
import sys
import socket

class hostnames(object):
	'''Scans the network specified (192.168.54.1 by default)'''
	def __init__(self, network='192.168.54.0'):
		'''network takes the network to be scanned, for example 192.168.1.0'''
		self.network = network
		self.nm = nmap.PortScanner()

	def scan(self):
		'''
		Carries out a nmap scan of the network
		Creates a list of the scan results in the format
		IP address : Host name
		'''
		self.nm.scan(hosts=self.network + '/24', arguments='-sP')
		self.host_list = [(x, self.nm[x]['hostnames']) for x in self.nm.all_hosts()]
		print("Hosts List \n", self.host_list)

	def print_hosts(self):
		'''Prints the list of devices, hostname and IP address to console'''
		print("Formatted List:")
		print("IP ADDRESS \tHost Name")
		for ipaddress in self.host_list:
			print("{0} \t{1}".format(ipaddress[0], ipaddress[1][0]['name']))

	def tidy_dict(self):
		'''Creates a dictionary of devices, hostname and IP '''
		self.tidy={}
		print("Tidy Dict:")
		for ipaddress in self.host_list:
			self.tidy[ipaddress[0]] = ipaddress[1][0]['name']
		print(self.tidy)

	def sort_dict(self):
		test = [x for x in self.tidy]

		print("Sorted list")
		print(test)
		#print(test.rsplit(".")[3])



if __name__ == "__main__":

	devices = hostnames() #enter ip range to scan, i.e. get_hostnames(192.168.1.0)
	devices.scan()
	devices.print_hosts()
	devices.tidy_dict()
#	devices.sort_dict()

	root= Tk()
	root.title("Ipswich Makerspace - Devices")
	root.geometry("500x300")
	Label(root, text= "IP Address" ,borderwidth=2,
		font="TkHeadingFont").grid(row=0,column=0,ipadx=20, sticky=W)
	Label(root, text= "Host Name",borderwidth=2,
		font="TkHeadingFont").grid(row=0,column=1,sticky=W)
	r = 1
	for ipaddress in devices.host_list:
		ip = StringVar()
		ip.set(ipaddress[0])
		host = StringVar()
		host.set(ipaddress[1][0]['name'])
		Label(root, textvariable= ip ,borderwidth=2).grid(row=r,column=0,
			ipadx=20, sticky=W)
		Label(root, textvariable= host,borderwidth=2).grid(row=r,column=1,
			sticky=W)
		r+=1

	root.mainloop()
	sys.exit()
